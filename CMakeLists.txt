cmake_minimum_required(VERSION 3.13)
project(asd_lex)

set(CMAKE_CXX_STANDARD 14)

add_executable(asd_lex main.cpp)