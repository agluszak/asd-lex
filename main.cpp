#include <iostream>
#include <vector>
#include <functional>

using namespace std;

int next_pow_of_2(int n) {
//    if (n == 1) {
//        return 0;
//    }
    int ret = 1;
    int x = 2;
    while (x < n) {
        ret++;
        x *= 2;
    }
    return ret;
}

int two_to_the_power_of(int exponent) {
    if (exponent == 0)
        return 1;

    if (exponent == 1)
        return 2;

    return 2 * two_to_the_power_of(exponent - 1);
}

// id, first, second
using triple = tuple<int, int, int>;

template<typename T>
void radix_sort(vector<T>& items, int n, function<int(T)> extractor) {
    vector<T> sorted(n);
    vector<int> counts(n + 1);
    for (int j = 0; j < n; ++j) {
        T p = items.at(j);
        int index = extractor(p);
        counts.at(index)++;
    }
    vector<int> positions_in_result(n + 1);
    for (int j = 1; j < n + 1; ++j) {
        positions_in_result.at(j) = positions_in_result.at(j - 1) + counts.at(j - 1);
    }
    for (int j = 0; j < n; ++j) {
        T p = items.at(j);
        int index = positions_in_result.at(extractor(p));
        ++positions_in_result.at(extractor(p));
        sorted.at(index) = p;
    }
    swap(sorted, items);
}

int main() {
    ios::sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    int size = next_pow_of_2(n);
    vector<int> s(n);
    cin.get();
    vector<vector<int>> dbf(size);
    dbf.at(0) = vector<int>(n);
    for (int i = 0; i < n; ++i) {
        dbf.at(0).at(i) = cin.get() - 'a';
    }
    for (int k = 1; k < size; ++k) {
        dbf.at(k) = vector<int>(n);
        vector<triple> triples;
        vector<int>& previous_level = dbf.at(k - 1);
        int offset = two_to_the_power_of(k - 1);
        for (int i = 0; i < n; ++i) {
            int first = previous_level.at(i);
            int second_index = i + offset;
            int second = second_index < n ? previous_level.at(second_index) : n;
            triples.emplace_back(i, first, second);
        }
        radix_sort<triple>(triples, n, [](triple p) { return get<2>(p); });
        radix_sort<triple>(triples, n, [](triple p) { return get<1>(p); });
        int counter = 0;
        triple t = triples.at(0);
        int id = get<0>(t);
        int previous_first = get<1>(t);
        int previous_second = get<2>(t);
        dbf.at(k).at(id) = counter;
        for (int j = 1; j < n; ++j) {
            triple t = triples.at(j);
            int id = get<0>(t);
            int first = get<1>(t);
            int second = get<2>(t);
            if (previous_first != first || previous_second != second) {
                counter++;
            }
            dbf.at(k).at(id) = counter;

            previous_first = first;
            previous_second = second;
        }
    }

    for (int j = 0; j < m; ++j) {
        int a, b, c, d;
        cin >> a >> b >> c >> d;
        a--;
        b--;
        c--;
        d--;
        int len = min(b - a + 1, d - c + 1);
        int level = next_pow_of_2(len) - 1;
        int result = 0;
        bool first_start_smaller = dbf.at(level).at(a) < dbf.at(level).at(c);
        if (first_start_smaller) {
            result = -1;
        } else {
            bool first_start_greater = dbf.at(level).at(a) > dbf.at(level).at(c);
            if (first_start_greater) {
                result = 1;
            } else {
                int first_index = a + len - two_to_the_power_of(level);
                int second_index = c + len - two_to_the_power_of(level);
                bool first_end_smaller = dbf.at(level).at(first_index) < dbf.at(level).at(second_index);
                if (first_end_smaller) {
                    result = -1;
                } else {
                    bool first_end_greater = dbf.at(level).at(first_index) > dbf.at(level).at(second_index);
                    if (first_end_greater) {
                        result = 1;
                    }
                }
            }
        }

        if (result == 0) {
            if (b - a > d - c) {
                result = 1;
            } else if (b - a < d - c) {
                result = -1;
            } else {
                result = 0;
            }
        }

        switch (result) {
            case 0:
                cout << "=" << endl;
                break;
            case 1:
                cout << ">" << endl;
                break;
            case -1:
                cout << "<" << endl;
                break;
        }

    }
    return 0;
}